@extends('layouts.app')

@section('title', 'Users')

@section('content')
@if(Session::has('notallowed'))
<div class = 'alert alert-danger'>
    {{Session::get('notallowed')}}
</div>
@endif
@if(Session::has('success'))
<div class = 'alert alert-success'>
    {{Session::get('success')}}
</div>
@endif
<div><a href =  "{{url('/users/create')}}"> Add new user</a></div>
<h1>List of users</h1>
<table class = "table table-dark">
    <tr>
        <th>id</th><th>Name</th><th>Email</th><th>Department</th><th></th><th>Role</th><th>View</th><th>Created</th><th>Updated</th>
    </tr>
    <!-- the table data -->
    @foreach($users as $user)
        <tr>
            <td>{{$user->id}}</td>
            <td>{{$user->name}}</td>
            <td>{{$user->email}}</td>
            <td>{{$user->department->name}}</td>
            <td>
                <a href = "{{route('users.makemanager',$user->id)}}">Make manager</a>
            </td> 
            <td>
            @foreach ($user->roles as $role)
                <div>{{$role->name}}</div>
            @endforeach
            </td> 
            <td>
                <a href = "{{route('users.view',$user->id)}}">View</a>
            </td> 
            <td>{{$user->created_at}}</td>
            <td>{{$user->updated_at}}</td>                                                            
        </tr>
    @endforeach
</table>
@endsection

