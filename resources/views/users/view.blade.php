@extends('layouts.app')

@section('title', 'View User')

@section('content')
@if(Session::has('success'))
<div class = 'alert alert-success'>
    {{Session::get('success')}}
</div>
@endif
<h1>User details</h1>
    <div class="form-group">
        <label for = "name">User name: {{$user->name}}</label>
    </div>     
    <div class="form-group">
        <label for = "email">User email: {{$user->email}}</label>
    </div>     
    <div class="form-group">
        <label for = "email">User department: {{$user->department->name}}</label>
    </div>
<h1>List of candidates</h1>
<table class = "table table-dark">
<tr>
        <th>id</th><th>Name</th><th>Email</th><th>Created</th><th>Updated</th>
    </tr>
    <!-- the table data -->
    @foreach($candidates as $candidate)
        <tr>
            <td>{{$candidate->id}}</td>
            <td>{{$candidate->name}}</td>
            <td>{{$candidate->email}}</td>                  
            <td>{{$candidate->created_at}}</td>
            <td>{{$candidate->updated_at}}</td>
            <td>
                    <a href = "{{route('candidate.removeUser',$candidate->id)}}">Remove candidate from user</a>
            </td>                                                               
        </tr>
    @endforeach
</table>
@endsection

